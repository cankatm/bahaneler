import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';

class MainCategoryButton extends Component {
    render() {
        const { content } = this.props;
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('SubCategories', { content })} >
                <View style={styles.mainCategoryButtonContainerStyle} >
                    <Text style={styles.mainCategoryButtonTextStyle} >{content}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(MainCategoryButton);