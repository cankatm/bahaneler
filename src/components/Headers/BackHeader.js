import React, { Component } from 'react';
import { 
    View, 
    Text,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';

class BackHeader extends Component {
    render() {
        return (
            <View style={styles.backHeaderContainerStyle}>
                <View style={styles.backHeaderBackButtonContainerStyle} >
                    <TouchableOpacity onPress={() => {this.props.onPress ? this.props.onPress() : this.props.navigation.goBack()}}>
                        <View style={styles.backHeaderBackButtonInnerContainerStyle} >
                            <Icon size={28} name='ios-arrow-back' color={colors.white} />
                        </View>
                    </TouchableOpacity>
                </View>
                
                <Text style={styles.headerTextStyle} >{this.props.headerText }</Text>
            </View>
        );
    }
}

export default withNavigation(BackHeader);