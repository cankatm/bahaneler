import React, { Component } from 'react';
import { 
    View, 
    Text,
    TouchableOpacity,
    Clipboard
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { BackHeader } from '../components/Headers';
import * as colors from '../helpers/ColorPalette';
import styles from './styles';




class ExcuseDetail extends Component {
    state = {
        showClipboardWarning: false
    }

    static navigationOptions = ({navigation}) => ({
        header: <BackHeader headerText='BAHANE' onPress={() => navigation.goBack()} />
    })

    render() {
        const copyToClipboard = () => {
            this.setState({ showClipboardWarning: true })
            Clipboard.setString(this.props.navigation.state.params.content);
            setTimeout(() => { this.setState({ showClipboardWarning: false }) }, 3000);
        }

        const renderClipboardNotification = () => {
            if (this.state.showClipboardWarning) {
                return (
                    <View style={styles.excuseDetailShareTextContainerStyle}>
                        <Text style={{ fontSize: 12, color: colors.white }} >Bahane kopyalandı.</Text>
                    </View>
                )
            }
        }

        return (
            <View style={{ flex: 1, backgroundColor: colors.lightBlue }} >
                <View style={styles.excuseDetailContentContainerStyle} >
                    <Text style={{ fontSize: 16, color: colors.white, paddingHorizontal: 32, textAlign: 'center' }} ><Text style={{ color: colors.orange}} >"  </Text>{this.props.navigation.state.params.content}<Text style={{ color: colors.orange}} >  "</Text></Text>
                </View>

                <View style={styles.excuseDetailIconsContainerStyle} >
                    <TouchableOpacity>
                        <View style={styles.excuseDetailSingleIconContainerStyle} >
                            <Icon size={24} name='facebook' color={colors.white} />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={styles.excuseDetailSingleIconContainerStyle}>
                            <Icon size={24} name='twitter' color={colors.white} />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => copyToClipboard()} >
                        <View style={styles.excuseDetailSingleIconContainerStyle}>
                            <Icon size={24} name='content-copy' color={colors.white} />
                        </View>
                    </TouchableOpacity>
                </View>

                {renderClipboardNotification()}

            </View>
        );
    }
}

export default withNavigation(ExcuseDetail);