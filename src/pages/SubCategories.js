import React, { Component } from 'react';
import { 
    View, 
    Text, 
    FlatList,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import { BackHeader } from '../components/Headers';
import { MainCategoryButton, ExcuseButton } from '../components/Buttons';
import * as colors from '../helpers/ColorPalette';
import styles from './styles';
import { subCategoriesArray } from '../helpers/DummyDatas';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

class SubCategories extends Component {
    static navigationOptions = ({navigation}) => ({
        header: <BackHeader headerText='BAHANELER' onPress={() => navigation.goBack()} />
    })

    render() {
        return (
            <View style={styles.subCategoriesContainerStyle} >
                <View style={styles.subCategoriesSecondHeaderContainerStyle} >
                    <Text style={styles.subCategoriesSecondHeaderTextStyle} >{this.props.navigation.state.params.content}</Text>
                </View>
                <FlatList
                    data={subCategoriesArray}
                    renderItem={({item}) => <ExcuseButton content={item.content} />}
                    keyExtractor={item => item.id}
                    style={{
                        height: WINDOW_HEIGHT - 60
                    }}
                />

                <View style={{ position: 'absolute', right: 16, bottom: 48 }} >
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AddExcuse', { content: this.props.navigation.state.params.content })} >
                        <View style={{ backgroundColor: colors.orange, width: 70, height: 70, alignItems: 'center', justifyContent: 'center', borderRadius: 45 }}>
                            <Icon size={40} name='ios-add' color={colors.white} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default withNavigation(SubCategories);