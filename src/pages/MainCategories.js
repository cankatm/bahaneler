import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    FlatList
} from 'react-native';

import { MainHeader } from '../components/Headers';
import { MainCategoryButton } from '../components/Buttons';
import styles from './styles';
import { mainCategoriesArray } from '../helpers/DummyDatas';

class MainCategories extends Component {
    render() {
        return (
            <View style={styles.mainCategoriesContainerStyle} >
                <View style={styles.subCategoriesSecondHeaderContainerStyle} >
                    <Text style={styles.subCategoriesSecondHeaderTextStyle} >Bir kategori seçin</Text>
                </View>
                <FlatList
                    data={mainCategoriesArray}
                    renderItem={({item}) => <MainCategoryButton content={item.content} />}
                    keyExtractor={item => item.id}
                />
            </View>
        );
    }
}

export default MainCategories;