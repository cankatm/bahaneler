export const mainCategoriesArray = [
    {
        id: '1',
        content: 'Ayrılık Bahaneleri'
    },
    {
        id: '2',
        content: 'Dersten Kaytarma Bahaneleri'
    },
    {
        id: '3',
        content: 'Temizlikten Kaçma Bahaneleri'
    },
    {
        id: '4',
        content: 'Geç Kalma Bahaneleri'
    },
    {
        id: '5',
        content: 'İşten Kaytarma Bahaneleri'
    },
    {
        id: '6',
        content: 'Randevu Ekme Bahaneleri'
    },
];

export const subCategoriesArray = [
    {
        id: '1',
        content: 'Senden daha iyilerine layığım.'
    },
    {
        id: '2',
        content: 'Çok iyi birisin ama bazı şeyleri dolduramıyorum...'
    },
    {
        id: '3',
        content: 'Seni üzüyorum.'
    },
    {
        id: '4',
        content: 'Benim için fazla iyisin.'
    },
    {
        id: '5',
        content: 'Başkası var.'
    },
    {
        id: '6',
        content: 'Benden daha iyilerine layıksın.'
    },
    {
        id: '7',
        content: 'Biz birbirimize göre değiliz!'
    },
]