import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const HEADER_HEIGHT = 80;
const ICON_CONTAINER_HEIGHT = 40;

export default StyleSheet.create({
    mainCategoryButtonContainerStyle: {
        width: WINDOW_WIDTH - 48,
        height: HEADER_HEIGHT,
        marginLeft: 24,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.lightestBlue,
        borderRadius: 12,
        marginVertical: 12
    },
    mainCategoryButtonTextStyle: {
        marginHorizontal: 16,
        color: colors.white,
        textAlign: 'center'
    },
});