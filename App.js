import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  StatusBar
} from 'react-native';

import { MainNavigator } from './src/helpers/PageStructure';
import * as colors from './src/helpers/ColorPalette';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <MainNavigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
