import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';

class ExcuseButton extends Component {
    render() {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ExcuseDetail', { content: this.props.content })} >
                <View style={styles.mainCategoryButtonContainerStyle} >
                    <Text style={styles.mainCategoryButtonTextStyle} >{this.props.content}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(ExcuseButton);