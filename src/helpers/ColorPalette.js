export const black = '#000';
export const white = '#fff';
export const red = 'red';
export const transparent = 'transparent';


export const lightestBlue = '#115478';
export const lightBlue = '#0b3e5f';
export const darkBlue = '#062847';
export const darkestBlue = '#00122e';

export const orange = '#FF8C00'

