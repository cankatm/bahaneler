import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TextInput,
    TouchableOpacity,
    ScrollView
} from 'react-native';

import { BackHeader } from '../components/Headers';
import * as colors from '../helpers/ColorPalette';
import styles from './styles';

// this.props.navigation.state.params.content geldiği ana başlığın adı
//this.state.bahaneText eklemek istediği bahane

class AddExcuse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bahaneText: ''
        }
    }

    static navigationOptions = ({navigation}) => ({
        header: <BackHeader headerText='Bahane Ekle' onPress={() => navigation.goBack()} />
    })

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.lightBlue }} >
                <ScrollView showsVerticalScrollIndicator={false} >
                    <View style={styles.addExcuseHeaderContainerStyle} >
                        <Text style={{ color: colors.white }} ><Text style={{ color: colors.orange }} >{this.props.navigation.state.params.content}</Text> kategorisine yeni bahane ekle:</Text>
                    </View>

                    <TextInput
                        {...this.props}
                        placeholder='Bahaneyi giriniz...'
                        maxLength = {300}
                        returnKeyType='done'
                        underlineColorAndroid={colors.transparent}
                        autoCorrect={false}
                        value={this.state.bahaneText}
                        onChangeText={ text => this.setState({ bahaneText: text })}
                        style={styles.addExcuseTextInputStyle}
                        multiline
                        blurOnSubmit
                        selectionColor={colors.orange}
                        placeholderTextColor={colors.lightestBlue}
                    />

                    <View style={styles.addExcuseConfirmButtonContainerStyle} >
                        <TouchableOpacity activeOpacity={0.75} >
                            <View style={styles.addExcuseConfirmButtonInnerContainerStyle} >
                                <Text style={styles.addExcuseConfirmButtonTextStyle}>EKLE</Text>   
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default AddExcuse;