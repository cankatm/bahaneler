import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    mainCategoriesContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT,
        flex: 1,
        backgroundColor: colors.lightBlue
    },
    subCategoriesContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT,
        flex: 1,
        backgroundColor: colors.lightBlue
    },
    subCategoriesSecondHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 12
    },
    subCategoriesSecondHeaderTextStyle: {
        fontSize: 14,
        color: colors.orange
    },
    addExcuseHeaderContainerStyle: {
        width: WINDOW_WIDTH - 48,
        marginLeft: 24,
        marginTop: 24
    },
    addExcuseTextInputStyle: {
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 14,
        paddingHorizontal: 8,
        paddingVertical: 8,
        width: WINDOW_WIDTH - 48,
        height: 400,
        marginLeft: 24,
        marginTop: 24,
        color: colors.lightBlue
    },
    addExcuseConfirmButtonContainerStyle: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        width: WINDOW_WIDTH - 48,
        marginLeft: 24,
        marginTop: 24
    },
    addExcuseConfirmButtonInnerContainerStyle: {
        backgroundColor: colors.orange,
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        height: 80,
        borderRadius: 40
    },
    addExcuseConfirmButtonTextStyle: {
        fontSize: 14,
        color: colors.white,
    },
    excuseDetailContentContainerStyle: { 
        width: WINDOW_WIDTH - 64,
        marginLeft: 32,
        backgroundColor: colors.lightestBlue,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 32,
        height: 300,
        borderWidth: 1,
        borderColor: colors.white
    },
    excuseDetailShareTextContainerStyle: { 
        width: WINDOW_WIDTH - 64,
        marginLeft: 32,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 16
    },
    excuseDetailIconsContainerStyle: { 
        flexDirection: 'row', 
        width: WINDOW_WIDTH - 128,
        marginLeft: 64,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    excuseDetailSingleIconContainerStyle: { 
        width: 48, 
        height: 48, 
        borderRadius: 24, 
        backgroundColor: colors.orange, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
});