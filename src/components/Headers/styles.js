import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const HEADER_HEIGHT = 60;
const ICON_CONTAINER_HEIGHT = 40;

export default StyleSheet.create({
    mainHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.darkBlue
    },
    backHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.darkBlue
    },
    backHeaderBackButtonContainerStyle: {
        width: ICON_CONTAINER_HEIGHT,
        height: ICON_CONTAINER_HEIGHT,
        position: 'absolute',
        left: 24,
        top: (HEADER_HEIGHT - ICON_CONTAINER_HEIGHT) / 2
    },
    backHeaderBackButtonInnerContainerStyle: {
        width: ICON_CONTAINER_HEIGHT,
        height: ICON_CONTAINER_HEIGHT,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    headerTextStyle: {
        fontSize: 14,
        color: colors.white
    },
});