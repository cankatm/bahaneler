import MainHeader from './MainHeader';
import BackHeader from './BackHeader';
import styles from './styles';

export { 
    MainHeader, 
    BackHeader, 
    styles 
};