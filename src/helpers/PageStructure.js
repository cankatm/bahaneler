import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    Animated, 
    Easing, 
    StatusBar 
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import { MainHeader } from '../components/Headers';

import * as colors from './ColorPalette';

import { 
    MainCategories, 
    SubCategories, 
    AddExcuse, 
    ExcuseDetail, 
} from '../pages';

export const MainNavigator = StackNavigator({
    MainCategories: { screen: MainCategories },
    SubCategories: { screen: SubCategories },
    AddExcuse: { screen: AddExcuse },
    ExcuseDetail: { screen: ExcuseDetail },
  },
  {
    headerMode: 'float',
    headerTransitionPreset: 'fade-in-place',
    lazyLoad: true,
    navigationOptions: ({navigation}) => ({
      header: <MainHeader />,
      gesturesEnabled: false
    })
  }
);