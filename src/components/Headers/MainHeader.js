import React, { Component } from 'react';
import { 
    View, 
    Text,
} from 'react-native';

import styles from './styles';

class MainHeader extends Component {
    render() {
        return (
            <View style={styles.mainHeaderContainerStyle} >
                <Text style={styles.headerTextStyle}>BAHANELER</Text>
            </View>
        );
    }
}

export default MainHeader;